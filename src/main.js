import { createApp } from 'vue'
import App from './App.vue'
import './index.css'

// import the fontawesome core
import { library } from '@fortawesome/fontawesome-svg-core'

// import specifc icons
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'


// add icons to the library
library.add(faUserSecret)

createApp(App).mount('#app')
