module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "bookmark-red": "#dc2626",
        "dark-gray-blue": "hsl(210, 10%, 33%)",
        "bookmark-red": "#FA5959",  
        "bookmark-blue": "#242A45",
        "bookmark-grey": "#9194A2",
        "bookmark-white": "#f7f7f7",
      },
      backgroundImage: {
        "hero-desktop": "url('/dist/imgs/bg-header-desktop.png')",
        "hero-mobile": "url('/assets/bg-header-mobile.png')",
      },
    },
    fontFamily: {
      Poppins: ["Poppins, sans-serif"],
    },
    container: {
      center: true,
      padding: '1rem',
      screens : {
        lg: "1124px",
        xl: "1124px",
        "2xl": "1124px",
      }
    },
  },
  plugins: [],
}